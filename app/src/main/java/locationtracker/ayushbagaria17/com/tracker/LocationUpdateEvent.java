package locationtracker.ayushbagaria17.com.tracker;

import android.location.Location;

/**
 * Created by ayush on 1/5/17.
 */


public class LocationUpdateEvent {
    Location location;

    public LocationUpdateEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
