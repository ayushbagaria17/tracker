package locationtracker.ayushbagaria17.com.tracker.ui.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by ayush on 4/5/17.
 */

public class BaseActivity extends AppCompatActivity {
    public ProgressDialog progress;

    public synchronized void showProgress() {
        progress =  new ProgressDialog(this);
        progress.setMessage("Loading...");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();

    }

    public synchronized void hideProgress() {
        if (progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }


    }

}
