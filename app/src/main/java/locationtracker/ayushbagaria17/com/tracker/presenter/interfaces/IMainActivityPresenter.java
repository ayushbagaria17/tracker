package locationtracker.ayushbagaria17.com.tracker.presenter.interfaces;

import locationtracker.ayushbagaria17.com.tracker.ui.activity.interfaces.IMainActivityView;

/**
 * Created by ayush on 4/5/17.
 */

public interface IMainActivityPresenter extends IBasePresenter<IMainActivityView> {
    void startShift();

    void updateMapIfShiftIsOn();

    void stopShift();
}
