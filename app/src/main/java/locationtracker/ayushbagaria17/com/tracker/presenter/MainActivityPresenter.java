package locationtracker.ayushbagaria17.com.tracker.presenter;

import android.location.Location;
import com.google.android.gms.maps.model.LatLng;
import java.util.ArrayList;
import java.util.List;
import locationtracker.ayushbagaria17.com.tracker.App;
import locationtracker.ayushbagaria17.com.tracker.EventBus;
import locationtracker.ayushbagaria17.com.tracker.LocationUpdateEvent;
import locationtracker.ayushbagaria17.com.tracker.presenter.interfaces.IMainActivityPresenter;
import locationtracker.ayushbagaria17.com.tracker.ui.activity.interfaces.IMainActivityView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ayush on 4/5/17.
 */

public class MainActivityPresenter implements IMainActivityPresenter {


    private IMainActivityView view;
    private EventBus bus;
    private Subscription busSubscritption;
    private CompositeSubscription subscriptions;
    private boolean isShiftOn = false;
    private List<LatLng> points;
    private long starttime;

    @Override
    public void attachView(IMainActivityView view) {
        this.view = view;
        this.subscriptions = new CompositeSubscription();
        this.bus = App.getEventBusInstance();
        subscribeEventBus();
        points = new ArrayList<>();
    }

    private void subscribeEventBus() {
        busSubscritption = bus.toObserverable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object o) {
                        if (o instanceof LocationUpdateEvent) {
                            updateLocation(((LocationUpdateEvent) o).getLocation());
                        }
                    }
                });
        subscriptions.add(subscriptions);
    }

    private void updateLocation(Location location) {
        if (location != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            points.add(latLng);
            redrawLine();
        }
    }



    private void redrawLine() {
        if (view != null) {
            updateTime();
            view.redrawLine(points);
        }
    }

    private void updateTime() {
        long timeGone = System.currentTimeMillis() - starttime;
        long timeGoneInSec = timeGone/1000;
        long timeGoneInMinutes = timeGoneInSec /60;
        int hr = 0;
        if (((int) timeGoneInMinutes)> 60) {
            long hrInLong = timeGoneInMinutes/60;
             hr = (int) hrInLong;
            timeGoneInMinutes = timeGoneInMinutes%60;
        }
        String hrString =  hr > 0 ? hr + "h " : "";
        String time = hrString + ((int) timeGoneInMinutes) + "m";
        view.updateTime(time);
    }

    @Override
    public void detachView() {
        view.stopLocationUpdate();
        points = null;
        this.view = null;
    }

    @Override
    public void rxUnsubscribe() {
        if (subscriptions != null && !subscriptions.isUnsubscribed()) {
            subscriptions.unsubscribe();
        }
    }

    @Override
    public void startShift() {
        this.isShiftOn = true;
        this.starttime = System.currentTimeMillis();
        view.startLocationUpdate();
        updateTime();
    }

    @Override
    public void updateMapIfShiftIsOn() {
        if (isShiftOn && view != null) {
            view.redrawLine(points);
        }
    }

    @Override
    public void stopShift() {
        //update time if required at the backend
        view.showSnapShot();
        view.stopLocationUpdate();
    }

//    public int solution(int[] A) {
//        System.out.println();
//       if (A.length == 1 || A.length ==0) {
//           return -1;
//       }
//       int[] temp = new int[A.length];
//        temp[0] =0;
//        for (int i = 1; i< A.length; i++) {
//            temp[i] = temp[i-1] + A[i-1];
//            System.out.println(i +""+ temp[i]);
//        }
//        if (temp[A.length-1] == 0) {
//            return A.length-1;
//        }
//        for (int j = A.length-2; j >=0; j--) {
//            temp[j] = temp[j+1] - A[j];
//            System.out.println(j +""+ temp[j]);
//            if (temp[j] == 0) {
//                return j;
//            }
//        }
//        return -1;
//    }
}
