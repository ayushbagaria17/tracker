package locationtracker.ayushbagaria17.com.tracker.ui.activity;

/**
 * Created by ayush on 4/5/17.
 */

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import locationtracker.ayushbagaria17.com.tracker.LocationService;
import locationtracker.ayushbagaria17.com.tracker.R;
import locationtracker.ayushbagaria17.com.tracker.presenter.MainActivityPresenter;
import locationtracker.ayushbagaria17.com.tracker.ui.activity.interfaces.IMainActivityView;

public class MainActivity extends BaseActivity implements IMainActivityView, OnMapReadyCallback {
    private static final int LOCATION_PERMISSION_ID = 1001;
    private GoogleMap mMap;
    private Polyline line;
    private Marker mymarker;

    @InjectView(R.id.tv_switch_text)
    TextView switchtext;

    @InjectView(R.id.ll_shift_time)
    LinearLayout shiftTimeContainer;

    @InjectView(R.id.switch_shift)
    Switch switchBtn;

    @InjectView(R.id.tv_shift_time)
    TextView shiftTime;
    private MainActivityPresenter presenter;
    private Dialog dialogSnapShot;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        init();
    }

    private void init() {
        initPresenter( new MainActivityPresenter());
        initView();
    }

    private void initView() {
        shiftTimeContainer.setVisibility(View.GONE);
        switchBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    presenter.startShift();
                } else {
                    presenter.stopShift();
                }
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        showProgress();
        // Keep the screen always on
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

    }

    public void initPresenter(MainActivityPresenter mainActivityPresenter) {
        presenter =mainActivityPresenter;
        presenter.attachView(this);

    }

    @Override
    public void redrawLine(List<LatLng> points) {
        PolylineOptions options = new PolylineOptions().width(7).color(Color.BLACK).geodesic(true);
        for (int i = 0; i < points.size(); i++) {
            LatLng point = points.get(i);
            options.add(point);
        }
        addMarker(points.get(points.size() - 1)); //add Marker in current position
        line = mMap.addPolyline(options);
    }

    private void addMarker(LatLng location) {
        if (mymarker != null) {
            mymarker.remove();
        }
        Marker tempMarker = mMap.addMarker(new MarkerOptions().position(location).title("Marker"));
        if (mymarker != null)
            mymarker.remove();
        mymarker = tempMarker;
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 13.0f));
    }

    @Override
    public void startLocationUpdate() {

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_ID);
            return;
        }
        mMap.setMyLocationEnabled(true);
        startLocationService();
    }

    @Override
    public void startLocationService() {
        switchtext.setText("Shift Started");
        Intent mIntent = new Intent(this, LocationService.class);
        bindService(mIntent, mConnection, BIND_AUTO_CREATE);
    }

    private boolean mBounded;
    private LocationService mLocationService;
    ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            mBounded = false;
            mLocationService = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            mBounded = true;
            LocationService.LocalBinder mLocalBinder = (LocationService.LocalBinder) service;
            mLocationService = mLocalBinder.getServerInstance();
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_ID && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
            startLocationService();
        } else {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mLocationService.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void stopLocationUpdate() {
        switchtext.setText("Start shift");
        if(mBounded) {
            unbindService(mConnection);
            mBounded = false;
        }
    }

    @Override
    public void updateTime(String time) {
        shiftTimeContainer.setVisibility(View.VISIBLE);
        shiftTime.setText(time);
    }

    @Override
    public void clearmap() {
        shiftTimeContainer.setVisibility(View.GONE);
        mMap.clear();
    }

    @Override
    public void showSnapShot() {
        dialogSnapShot = new Dialog(this);
        dialogSnapShot.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogSnapShot.setContentView(R.layout.snapshot_dialog);

        dialogSnapShot.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                clearmap();
            }
        });
        Button btnOkay = (Button) dialogSnapShot.findViewById(R.id.btn_okay);
        btnOkay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogSnapShot.dismiss();
            }
        });
        final GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                ImageView imageView = (ImageView) dialogSnapShot.findViewById(R.id.iv_screenShot);
                imageView.setImageBitmap(snapshot);
                dialogSnapShot.show();
            }
        };
        mMap.snapshot(callback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.updateMapIfShiftIsOn();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        hideProgress();
        LatLng location = new LatLng( 12.972442,77.580643);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(location));
    }

    @Override
    public void showProgressBar() {
        showProgress();
    }

    @Override
    public void hideProgressBar() {
        hideProgress();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showErrorDialog(String string) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        presenter.rxUnsubscribe();
    }
}
