package locationtracker.ayushbagaria17.com.tracker.presenter.interfaces;

/**
 * Created by ayush on 4/5/17.
 */

public interface IBasePresenter<V> {
    void attachView(V view);
    void detachView();
    void rxUnsubscribe();
}
