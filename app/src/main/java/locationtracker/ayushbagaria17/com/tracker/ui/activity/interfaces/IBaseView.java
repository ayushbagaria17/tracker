package locationtracker.ayushbagaria17.com.tracker.ui.activity.interfaces;

import android.content.Context;

/**
 * Created by ayush on 4/5/17.
 */

public interface IBaseView {
    void showProgressBar();

    void hideProgressBar();

    Context getContext();

    void showErrorDialog(String string);

}
