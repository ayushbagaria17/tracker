package locationtracker.ayushbagaria17.com.tracker;

import android.app.Application;

/**
 * Created by ayush on 1/5/17.
 */

public class App extends Application {
    private static EventBus bus;
    public static EventBus getEventBusInstance(){
        if (bus == null) {
            bus = new EventBus();
        }
        return bus;
    }
}