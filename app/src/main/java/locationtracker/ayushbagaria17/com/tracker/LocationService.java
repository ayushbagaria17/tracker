package locationtracker.ayushbagaria17.com.tracker;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import io.nlopez.smartlocation.location.config.LocationAccuracy;
import io.nlopez.smartlocation.location.config.LocationParams;
import io.nlopez.smartlocation.location.providers.LocationGooglePlayServicesProvider;

/**
 * Created by ayush on 1/5/17.
 */

public class LocationService extends Service implements OnLocationUpdatedListener {

    private LocationGooglePlayServicesProvider provider;
    IBinder mBinder = new LocalBinder();
    EventBus bus;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onLocationUpdated(Location location) {
        bus.send(new LocationUpdateEvent(location));
    }

    public class LocalBinder extends Binder {
        public LocationService getServerInstance() {
            return LocationService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        provider = new LocationGooglePlayServicesProvider();
        provider.setCheckLocationSettings(true);
        bus = App.getEventBusInstance();
        SmartLocation smartLocation = new SmartLocation.Builder(this).logging(true).build();
        LocationParams params = new LocationParams.Builder().setAccuracy(LocationAccuracy.HIGH).setDistance(1).setInterval(2000).build();
        smartLocation.location(provider).config(params).start(this);
       while (smartLocation.location().get().getLastLocation() != null) {
            bus.send(new LocationUpdateEvent(smartLocation.location().get().getLastLocation()));
           break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (provider != null) {
            provider.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SmartLocation.with(this).location().stop();

    }
}
