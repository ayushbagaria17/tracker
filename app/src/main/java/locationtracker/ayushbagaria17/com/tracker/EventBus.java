package locationtracker.ayushbagaria17.com.tracker;

import rx.Observable;
import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;
import rx.subjects.Subject;

/**
 * Created by ayush on 1/5/17.
 */

public class EventBus {
    private final Subject<Object, Object> bus = new SerializedSubject<>(PublishSubject.create());


    public EventBus() {

    }

    public void send(Object o) {
        bus.onNext(o);
    }

    public Observable<Object> toObserverable() {
        return bus;
    }

    public boolean hasObservers() {
        return bus.hasObservers();
    }

}