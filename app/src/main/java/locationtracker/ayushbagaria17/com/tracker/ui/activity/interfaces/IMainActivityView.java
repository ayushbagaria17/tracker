package locationtracker.ayushbagaria17.com.tracker.ui.activity.interfaces;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by ayush on 4/5/17.
 */

public interface IMainActivityView extends IBaseView {
    void startLocationUpdate();

    void startLocationService();

    void redrawLine(List<LatLng> points);

    void stopLocationUpdate();

    void updateTime(String time);

    void clearmap();

    void showSnapShot();
}
