package locationtracker.ayushbagaria17.com.tracker;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import locationtracker.ayushbagaria17.com.tracker.presenter.MainActivityPresenter;
import locationtracker.ayushbagaria17.com.tracker.ui.activity.MainActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by ayush on 5/5/17.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);
    private MainActivityPresenter presenter;
    private MainActivity mainActivity;

    @Before
    public void setUp() throws Exception {
        mainActivity = rule.getActivity();
        presenter = mock(MainActivityPresenter.class);
    }


    @Test
    public void ensureMapIsPresent() {
        assertThat(mainActivity.findViewById(R.id.map), notNullValue());
    }

    @Test
    public void ensureSwitchIsPresent() {
        assertThat(mainActivity.findViewById(R.id.switch_shift), notNullValue());
    }

    @Test
    public void presenterStartShiftIsCalled() {
        mainActivity.initPresenter(presenter);
        onView(withId(R.id.switch_shift)).perform(swipeRight());
        verify(presenter,times(1)).startShift();
    }

    @Test
    public void presenterStopShiftIsCalled() {
        mainActivity.initPresenter(presenter);
        onView(withId(R.id.switch_shift)).perform(swipeRight());
        onView(withId(R.id.switch_shift)).perform(swipeLeft());
        verify(presenter,times(1)).stopShift();
    }

    @Test
    public void timerIsShownWhenShiftIsOn() {
        MainActivityPresenter tempPresenter = new MainActivityPresenter();
        mainActivity.initPresenter(tempPresenter);
        onView(withId(R.id.switch_shift)).perform(swipeRight());
        assertThat(mainActivity.findViewById(R.id.ll_shift_time),isDisplayed());
    }

    @Test
    public void timerShouldHideWhenShiftStops() {
        MainActivityPresenter tempPresenter = new MainActivityPresenter();
        mainActivity.initPresenter(tempPresenter);
        onView(withId(R.id.switch_shift)).perform(swipeRight());
        onView(withId(R.id.switch_shift)).perform(swipeLeft());
        onView(withId(R.id.ll_shift_time)).check(doesNotExist());
    }


//    @Test
//    public void
//    @Test
//    public void ensureSwitchIsPresent() {
//        MainActivity mainActivity = rule.getActivity();
//        assertThat(mainActivity.findViewById(R.id.switch_shift), notNullValue());
//    }



}
