package locationtracker.ayushbagaria17.com.tracker;

import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import locationtracker.ayushbagaria17.com.tracker.presenter.MainActivityPresenter;
import locationtracker.ayushbagaria17.com.tracker.ui.activity.interfaces.IMainActivityView;
import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.plugins.RxJavaPlugins;
import rx.schedulers.Schedulers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ayush on 5/5/17.
 */

public class MainActivityPresenterTest {
    private IMainActivityView view;
    private MainActivityPresenter presenter;
    private EventBus bus;

    @Before
    public void setup() {
        RxAndroidPlugins.getInstance().reset();
        this.view = mock(IMainActivityView.class);
        presenter = new MainActivityPresenter();
        this.bus = App.getEventBusInstance();
        RxAndroidPlugins.getInstance().registerSchedulersHook(new RxAndroidSchedulersHook() {
            @Override
            public Scheduler getMainThreadScheduler() {
                return Schedulers.immediate();
            }
        });
        presenter.attachView(view);
    }

    @Test
    public void locationUpdateShouldStartWhenShiftIsOn() {
        presenter.startShift();
        verify(view,times(1)).startLocationUpdate();
    }

    @Test
    public void locationUpdateShouldStopWhenShiftStops() {
        presenter.stopShift();
        verify(view,times(1)).stopLocationUpdate();
    }

    @Test
    public void takeSnapShotWhenShiftStops() {
        presenter.stopShift();
        verify(view,times(1)).showSnapShot();
    }

    @Test
    public void redrawLineIfShiftIsOn() {
        presenter.startShift();
        presenter.updateMapIfShiftIsOn();
        verify(view,times(1)).redrawLine(anyListOf(LatLng.class));
    }

    @Test
    public void ifViewNullDontRedrawLine() {
        presenter.startShift();
        presenter.attachView(null);
        presenter.updateMapIfShiftIsOn();
        verify(view,times(0)).redrawLine(anyListOf(LatLng.class));
    }

    @Test
    public void ifShiftNotStartedDontRedrawLine() {
        presenter.updateMapIfShiftIsOn();
        verify(view,times(0)).redrawLine(anyListOf(LatLng.class));
    }

    @Test
    public void updateLocationWhenANewEventComes() {
        Location location =mock(Location.class);
        when(location.getLatitude()).thenReturn(12.972442);
        when(location.getLongitude()).thenReturn(77.580643);
        bus.send(new LocationUpdateEvent(location));
        verify(view,times(1)).redrawLine(anyListOf(LatLng.class));
    }

    @Test
    public void updateTimeWhenANewEventComes() {
        Location location =mock(Location.class);
        when(location.getLatitude()).thenReturn(12.972442);
        when(location.getLongitude()).thenReturn(77.580643);
        bus.send(new LocationUpdateEvent(location));
        verify(view,times(1)).updateTime(any(String.class));
    }

    @Test
    public void stopLocationUpdateWhenViewIsDetached() {
        presenter.detachView();
        verify(view,times(1)).stopLocationUpdate();
    }

    @Test
    public void ShouldNotReceiveEventWhenRxUnsubscribed() {
        presenter.rxUnsubscribe();
        Location location =mock(Location.class);
        when(location.getLatitude()).thenReturn(12.972442);
        when(location.getLongitude()).thenReturn(77.580643);
        bus.send(new LocationUpdateEvent(location));
        verify(view,times(1)).redrawLine(anyListOf(LatLng.class));
    }


}
